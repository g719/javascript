const myForm = document.getElementById("form");
const email = document.getElementById("email");

// Executes modal and animation when form is submitted, after email is checked!
myForm.addEventListener("submit", (e) => {
  correctInputs();
  e.preventDefault();
});

// Open modal, Animation
const sendEvent = () => {
  document.querySelector("#modal-content").innerHTML = `
        <i class="fa fa-envelope" id="envelope-icon"></i>`;
  document.querySelector("#modal-wrapper").style.display = "flex";
  document.querySelector("#envelope-icon").style.animation =
    "envelope 2s ease-in-out";
  document
    .querySelector("#envelope-icon")
    .addEventListener("animationend", endFunction);
};

// Close modal
const modalClose = () => {
  document.querySelector("#modal-wrapper").style.display = "none";
  myForm.reset(); // Clears the form input
};

// After animation
const endFunction = () => {
  document.querySelector("#envelope-icon").style.display = "none";
  document.querySelector(
    "#modal-content"
  ).innerHTML = `<p id="messageSent">Message sent!</p>`;
  setTimeout(() => {
    document.querySelector(
      "#modal-content"
    ).innerHTML = `<p id="thankYouMessage">Thank You!</p>
    <i onclick="modalClose()" class="fa fa-times" id="modal-close-button"></i>`;
  }, 1000);
};

// Email regular expression function
const checkEmail = (email) => {
  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};
// Checking email
const correctInputs = () => {
  const emailInput = email.value;
  try {
    if (!checkEmail(emailInput)) {
      throw "Not a valid email!";
    } else {
      sendEvent();
    }
  } catch (error) {
    document.querySelector(
      "#modal-content"
    ).innerHTML = `<p id="invalidEmail">${error}</p>
    <i onclick="modalClose()" class="fa fa-times" id="modal-close-button"></i>`;
    document.querySelector("#modal-wrapper").style.display = "flex";
  }
};
