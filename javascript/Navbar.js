const hamburger = document.querySelector(".hamburger");
const navMenu = document.querySelector(".nav-menu");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
}
const navLink = document.querySelectorAll(".nav-link");

navLink.forEach(n => n.addEventListener("click", closeMenu));

function closeMenu() {
    hamburger.classList.remove("active");
    navMenu.classList.remove("active");
}


const navbar = document.querySelector(".navbar"); 
const target = document.querySelector("#about-us"); 


window.addEventListener("load", function () {
  document.addEventListener("scroll", () => {
    if (window.scrollY >= target.getBoundingClientRect().top) {
      navbar.style.padding = "10px 40px";
    } else {
      navbar.style.padding = "40px";
    }
  });
});