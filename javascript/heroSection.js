/* ----- Hero Image ----- */

//Array of the images for the hero section
const heroImages = [
  {
      filename: "morning.jpg",
      alt: "nature in morning light"
  },
  {
      filename: "day.jpg",
      alt: "desert"
  },
  {
      filename: "afternoon.jpg",
      alt: "snowy mountains"
  },{
      filename: "night.jpg",
      alt: "stary sky over mountain"
  },
];

//Based on the time of day(hour), the hero image's src and alt are set.
const setHeroImage = () => {
  const getHour = () => {
    const date = new Date();
    return date.getHours();
  }

  const hour = getHour();
  const heroImage = document.getElementById("hero-image");

  if (hour > 5 && hour < 11){
    heroImage.src = `./img/${heroImages[0].filename}`;
    heroImage.alt = `${heroImages[0].alt}`;
  } else if (hour > 10 && hour < 15) {
    heroImage.src = `./img/${heroImages[1].filename}`;
    heroImage.alt = `${heroImages[1].alt}`;
  } else if (hour > 14 && hour < 20) {
    heroImage.src = `./img/${heroImages[2].filename}`;
    heroImage.alt = `${heroImages[2].alt}`;
  } else {
    heroImage.src = `./img/${heroImages[3].filename}`;
    heroImage.alt = `${heroImages[3].alt}`;
  }
}

/* ----- Count down ----- */

//A date and time for the count down is stated and the variable holds it in milliseconds
const countDownDate = new Date("Jan 1, 2022 00:00:00").getTime();

//Runs the countDown function every second to make the count down tick down per second.
const x = setInterval(() => {
  countDown();
}, 1000);

//The time left until countDownDate is calculated
const countDown = () => {
  const date = new Date();

  const now = date.getTime(); //current time in milliseconds
  
  let difference = countDownDate - now;

  //Calculations
  let days = Math.floor(difference / (1000 * 60 * 60 * 24));
  let hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((difference % (1000 * 60)) / 1000);

  //Renders the count down on site
  document.getElementById("count-down").innerHTML = 
    `${days} days ${hours} hours ${minutes} min ${seconds} sec`;

  if (difference < 0) { //this code block runs if the countdown has expired.
    clearInterval(x);
    document.getElementById("count-down").innerHTML = `Happy New Year!`;
  }
}

/* ----- Zoom effect ----- */

//Targets the h1 count-down for zoom on scroll effect.
const zoomElement = document.querySelector("#count-down");
let zoom = 1; //starting from scale 1
const zoomSpeedIn = 0.1;
const zoomSpeedOut = 0.2;

document
  .querySelector(".hero-section-overlay")
  .addEventListener("wheel", (e) => {
    if (e.deltaY > 0 && zoom < 20) { //deltaY tracks current position on the page on the verticle axis so zoom in as soon as we start to scroll in this case
      zoomElement.style.transform = `scale(${(zoom += zoomSpeedIn)})`; //zooms in
    } else if (zoom < 0.8) {
      zoomElement.style.transform = `scale(${zoom})`; //text should not be too small
    } else {
      zoomElement.style.transform = `scale(${(zoom -= zoomSpeedOut)})`; //zooms out
    }
  });

//the setHeroImage function is called after page is loaded as well as the countDown
window.addEventListener("load", setHeroImage);
window.addEventListener("load", countDown);