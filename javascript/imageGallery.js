const current = document.querySelector("#current");

// querySelectorAll för att det finns mer flera bilder
const images = document.querySelectorAll(".images img");
const imagesTwo = document.querySelectorAll(".images-two img");

const opacity = 0.6;


images.forEach((img) => img.addEventListener("click", imgClick));

function imgClick(e) {
  // loopar igenom och återställer opacity när en bild har klickats
  images.forEach((img) => (img.style.opacity = 1));

  //byter tillfälliga bilden src till den nya iklickade bildens src
  current.src = e.target.src;

  // Lägger till fade in class på den bild som klickats på
  current.classList.add("fade-in");

  // setTimeout Efter 0,5 sekunder ska fade in tas bort på bilden och appliceras på nästa bild som klickas
  setTimeout(() => current.classList.remove("fade-in"), 500);

  // Ifall target inte har class attributet imgTwo så ändras opacity på varje klick 
  if(e.target.getAttribute("class") !== "imgTwo"){
    e.target.style.opacity = opacity;
  }
}




//Modal kod

// Öppnar upp modalen
function openModal() {
  document.querySelector("#modal").style.display = "block";

  // När modalen öppnas göms images-two classen och endast modelen synns.
  document.querySelector(".images-two").style.display = "none";
}

// Stänger Modalen
const closeimagestwo = () => {
  document.querySelector("#modal").style.display = "none";
  document.querySelector(".images-two").style.display = "";
};

 imagesTwo.forEach((img) => img.addEventListener("click", imgClick));